# Plex To Emby Migration
This project contains a few python scripts that will help you migrate some of the data from your plex server to you emby server. The scripts match using IMDB, TVDB , and TMDB.  Right now there are two main scripts.

The first one is named metadata-migration.py . It will migrate the following data:

- Posters
- Title
- Original Title
- Sort Title
- Parental Rating
- Labels/Tags
 

The second one is named watched-history-migration.py . This script can migrate watched history for any user but there is a caveat. You have to be able to get that user's plex token. Checkout the readme in the project repository for more details. It will migrate the following data:

- Watched History

There are a few things that you will to be aware of. Due to how the plex api works you can't pull in the watched history of users that you share your server with using the admin plex api key. If you want to migrate multiple users you will need to run this script separetly for each user. You will need to get the plex api token for each user by using this guide:

https://support.plex.tv/articles/204059436-finding-an-authentication-token-x-plex-token/

One other think to note is the script doesn't always find matches for specials. It tries to match IMDB/TVDB/TMDB IDs.

You will need to update the following values at the top of each script before you try and run them.

```
PLEX_URL = "http://localhost:32400/"
PLEX_TOKEN = ""
PLEX_SERVER_NAME = "SomeFriendlyName"
PLEX_MOVIE_LIBRARIES = "Movies"
PLEX_SERIES_LIBRARIES = "TV Shows"
EMBY_URL = "http://localhost:8096/emby/"
EMBY_APIKEY = ""
EMBY_USERNAME = ""
```
